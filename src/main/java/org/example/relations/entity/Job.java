package org.example.relations.entity;

public enum Job {

    PROFESSOR, ENGINEER, LAWYER, DOCTOR, ARCHITECT, ACCOUNTANT

}
