package org.example.relations.entity;


import jakarta.persistence.*;

@Entity

@Table(name="children")
public class Child {

    @Id // primary key for the table
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    private String name;

    // Presupun ca fiecare copil are o singura mancare favorita
    // Presupun ca fiecare mancare este favorita unui singur copil


    @OneToOne() //// foreign key for linking tables
    private Food favouriteFood ;

    @OneToOne(cascade = {CascadeType.PERSIST, CascadeType.REMOVE})
    private Toy favouriteToy;

    public Child(Integer id, String name, Food favouriteFood, Toy favouriteToy) {
        this.id = id;
        this.name = name;
        this.favouriteFood = favouriteFood;
        this.favouriteToy = favouriteToy;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Food getFavouriteFood() {
        return favouriteFood;
    }

    public void setFavouriteFood(Food favouriteFood) {
        this.favouriteFood = favouriteFood;
    }

    public Toy getToy() {
        return favouriteToy;
    }

    public void setToy(Toy toy) {
        this.favouriteToy = toy;
    }
}
