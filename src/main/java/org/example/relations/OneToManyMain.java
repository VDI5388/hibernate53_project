package org.example.relations;

import org.example.databaseConfiguration.DatabaseConfig;
import org.example.exercitiu1.Genre;
import org.example.relations.entity.*;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;


import java.util.List;

public class OneToManyMain {

    public static void main(String[] args) {


        SessionFactory sessionFactory = DatabaseConfig.getSessionFactory();


        Session session = sessionFactory.openSession();
        Transaction t1 = session.beginTransaction();

        Hobby h1 = new Hobby(null, "Tennis", "beginner");

        Hobby h2 = new Hobby(null, "Chess", "intermediate");

        TvShow bones = new TvShow(null, "Bones", Genre.ACTION_SCI_FI);
        TvShow dosareleX = new TvShow(null, "DosareleX", Genre.ACTION);
        // Nu vrem sa persistam TV Show-urile deoarece operatia de persist este cascadata din Mother

        Mother m1 = new Mother(null, "Doina",Job.PROFESSOR, List.of(h1,h2),List.of(bones,dosareleX));

        session.persist(h1);
        session.persist(h2);
        session.persist(m1);

        t1.commit();
        session.close();

    }
}