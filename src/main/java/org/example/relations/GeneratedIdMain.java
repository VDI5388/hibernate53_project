package org.example.relations;

import org.example.databaseConfiguration.DatabaseConfig;
import org.example.relations.entity.*;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

public class GeneratedIdMain {

    public static void main(String[] args) {

        SessionFactory sessionFactory = DatabaseConfig.getSessionFactory();

        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();

        Mother mom = new Mother(null, "Doina", Job.PROFESSOR);

        session.persist(mom);

        Mother mom2 = new Mother(null, "Luminita", Job.ACCOUNTANT);

        System.out.println(mom2.getId());

        session.persist(mom2);

        System.out.println(mom2.getId());

        transaction.commit();
        session.close();

    }

}
