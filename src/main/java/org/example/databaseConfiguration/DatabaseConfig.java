package org.example.databaseConfiguration;

import org.example.entity.Car;
import org.example.entity.Driver;
import org.example.entity.Truck;
import org.example.exercitiu1.Actor;
import org.example.exercitiu1.Movie;
import org.example.exercitiu2.Student;
import org.example.relations.entity.*;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class DatabaseConfig {

    private static SessionFactory sessionFactory = null;

    private DatabaseConfig() {

    }
        public static SessionFactory getSessionFactory() {

            if (sessionFactory == null) {

                sessionFactory = new Configuration()
                        .configure("hibernate.config.xml")
                        .addAnnotatedClass(Car.class)
                        .addAnnotatedClass(Truck.class)
                        .addAnnotatedClass(Truck.class)
                        .addAnnotatedClass(Driver.class)
                        .addAnnotatedClass(Actor.class)
                        .addAnnotatedClass(Movie.class)
                        .addAnnotatedClass(Student.class)
                        .addAnnotatedClass(Child.class)
                        .addAnnotatedClass(Food.class)
                        .addAnnotatedClass(Hobby.class)
                        .addAnnotatedClass(Job.class)
                        .addAnnotatedClass(Mother.class)
                        .addAnnotatedClass(Toy.class)
                        .addAnnotatedClass(TvShow.class)
                        .addAnnotatedClass(Animal.class)
                        .addAnnotatedClass(Owner.class)
                        .buildSessionFactory();
            }

            return  sessionFactory;

    }

}
