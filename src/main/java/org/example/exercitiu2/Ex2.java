package org.example.exercitiu2;

import org.example.databaseConfiguration.DatabaseConfig;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

public class Ex2 {

    public static void main(String[] args) {


        SessionFactory sessionFactory = DatabaseConfig.getSessionFactory();


        Session session = sessionFactory.openSession();

        Transaction t1 = session.beginTransaction();

        Student student1 = new Student(12, "Mihai", 1995);

        session.persist(student1);

        t1.commit();

        session.close();

    }
}