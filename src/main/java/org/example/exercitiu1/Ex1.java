package org.example.exercitiu1;

import org.example.databaseConfiguration.DatabaseConfig;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import static org.example.databaseConfiguration.DatabaseConfig.getSessionFactory;

public class Ex1 {

    // adaugam 2 filme si 3 actori
    // creeare session factory

    // creare tabele
    // inserare date


    public static void main(String[] args) {

        SessionFactory sessionFactory = DatabaseConfig.getSessionFactory();

        Session session = getSessionFactory().openSession();

        Transaction transaction = session.beginTransaction();

        Movie movie1 = new Movie();

        movie1.setId(1);
        movie1.setName("LOTR");
        movie1.setIncasari(377.027);
        movie1.setGenre(Genre.ACTION);
        session.persist(movie1);

        Movie movie2 = new Movie();

        movie2.setId(2);
        movie2.setName("The Matrix");
        movie2.setIncasari(172.076);
        movie2.setGenre(Genre.ACTION_SCI_FI);

        session.persist(movie2);

        Actor actor1 = new Actor();

        actor1.setId(1);
        actor1.setName("Gandalf");
        actor1.setAge(24000);
        session.persist(actor1);

        Actor actor2 = new Actor();

        actor2.setId(2);
        actor2.setName("Keanu Reeves");
        actor2.setAge(42);

        session.persist(actor2);

        Actor actor3 = new Actor();

        actor3.setId(3);
        actor3.setName("Scarlett Johansson");
        actor3.setAge(39);

        session.persist(actor3);



        transaction.commit();
        session.close();
    }
}
