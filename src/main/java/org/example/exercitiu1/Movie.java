
package org.example.exercitiu1;
// id
// nume
// release date
// incasari
// gen
// imdb score


import jakarta.persistence.*;

import java.util.Date;

@Entity
public class Movie {
@Id
        private Integer id;
        private String name;
        @Column(name="release_date")
        private Date releaseDate;

        @Enumerated(value = EnumType.STRING)
        private Genre genre;

        @Column(name="imdb_score")
        private Integer imdbScore;

        private Double incasari;

        public Integer getId() {
                return id;
        }

        public void setId(Integer id) {
                this.id = id;
        }

        public String getName() {
                return name;
        }

        public void setName(String name) {
                this.name = name;
        }

        public Date getReleaseDate() {
                return releaseDate;
        }

        public void setReleaseDate(Date releaseDate) {
                this.releaseDate = releaseDate;
        }

        public Genre getGenre() {
                return genre;
        }

        public void setGenre(Genre genre) {
                this.genre = genre;
        }

        public Integer getImdbScore() {
                return imdbScore;
        }

        public void setImdbScore(Integer imdbScore) {
                this.imdbScore = imdbScore;
        }

        public Double getIncasari() {
                return incasari;
        }

        public void setIncasari(Double incasari) {
                this.incasari = incasari;
        }
}
