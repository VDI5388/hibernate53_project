package org.example.entity;

import org.example.databaseConfiguration.DatabaseConfig;
import org.example.entity.Car;
import org.example.entity.Driver;
import org.example.entity.Truck;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

public class Main {
    public static void main(String[] args) {

        SessionFactory sessionFactory = DatabaseConfig.getSessionFactory();

        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();

        Car c = new Car();
        c.setId(11);
        c.setName("Test car");
        session.persist(c); // Persist e un fel de insert in tabel !!!

        Truck t = new Truck();
        t.setId(2);
        t.setName("Test truck");
        session.persist(t); // Persist e un fel de insert in tabel !!!



        /*
Creați un tabel pentru șoferi (drivers)

id - primary key
name - not null
max_traveldistance - integer
email - unic
security_key - string max 30 caractere - de facut security key-ul unic!!!


 */

        Driver d = new Driver();
        d.setId(7);
        d.setName("Johnathan");
        d.setEmail("verzes.ionut@gmail.com");
        d.setMaxTravelDistance(1000);
        d.setSecurityKey("VDI112233");
        session.persist(d);

        transaction.commit();
        session.close();

    }

}